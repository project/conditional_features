Introduction
------------
Conditional Features provides the ability to enable or disable modules based
on conditions set by the site administrator.

Features
--------
It utilises Condition Plugin API in the same manner as Drupal entities such
as Blocks are able to do.

It also provides its own Condition plugin which enables Blocks and other
entities that take advantage of the Condition Plugin API to be enabled or
disabled based on Conditional Features.

Installation
------------
Install using composer in the usual fashion:

`composer require drupal/conditional_features`

Post-Installation
-----------------
After installation, visit `/admin/structure/conditional-features` to create
and manage conditional features.

A Conditional Features Manager service is provided to allow developers to
interact with conditional features that have been created.

Get instance of the Conditional Features Manager:
```php
$conditional_features_manager = \Drupal::service('conditional_features.manager');
```

Get an array of all conditional features:
```php
$features = $conditional_features_manager->getFeatures();
```

Get an array of only enabled conditional features:
```php
$features = $conditional_features_manager->getEnabledFeatures();
```

Test is a conditional feature is enabled (by ID);
```php
$state = $conditional_features_manager->isEnabled('disabled_feature');
$state = $conditional_features_manager->isEnabled('enabled_feature');
```


Similar projects
----------------
[Feature Flags](https://www.drupal.org/project/featureflags): Possibly feature
complete but doesn't appear to be supported any longer (over a year since last
update).

[Feature Toggle](https://www.drupal.org/project/feature_toggle): Doesn't appear
to be supported/maintained any longer (over a year since last updates, lots of
open tickets that haven't had any attention)

[Switches](https://www.drupal.org/project/switches): Maintainer has suggested
this project is abandoned and to use one of the alternatives.
