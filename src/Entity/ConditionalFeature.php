<?php

namespace Drupal\conditional_features\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the conditional feature entity type.
 *
 * @ConfigEntityType(
 *   id = "conditional_feature",
 *   label = @Translation("Conditional feature"),
 *   label_collection = @Translation("Conditional features"),
 *   label_singular = @Translation("conditional feature"),
 *   label_plural = @Translation("conditional features"),
 *   label_count = @PluralTranslation(
 *     singular = "@count conditional feature",
 *     plural = "@count conditional features",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\conditional_features\ConditionalFeaturesListBuilder",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "form" = {
 *       "add" = "Drupal\conditional_features\Form\ConditionalFeatureForm",
 *       "edit" = "Drupal\conditional_features\Form\ConditionalFeatureForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider::class,
 *     }
 *   },
 *   config_prefix = "conditional_feature",
 *   admin_permission = "administer conditional_feature",
 *   links = {
 *     "canonical" = "/admin/structure/conditional-features/{conditional_feature}",
 *     "collection" = "/admin/structure/conditional-features",
 *     "add-form" = "/admin/structure/conditional-features/add",
 *     "edit-form" = "/admin/structure/conditional-features/{conditional_feature}/edit",
 *     "delete-form" = "/admin/structure/conditional-features/{conditional_feature}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "description",
 *   },
 * )
 */
final class ConditionalFeature extends ConfigEntityBase implements ConditionalFeatureInterface {

  /**
   * The Conditional Feature ID.
   */
  protected string $id;

  /**
   * The Conditional Feature name.
   */
  protected string $name;

  /**
   * The Conditional Feature description.
   */
  protected string $description;

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return \Drupal::service('conditional_features.state_manager')->get($this->id(), FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled(bool $state) {
    \Drupal::service('conditional_features.state_manager')->set($this->id(), $state);
  }

}
