<?php

namespace Drupal\conditional_features\Entity;

/**
 * Provides an interface defining a Conditional Feature entity type.
 */
interface ConditionalFeatureInterface {

  /**
   * Returns the Conditional Feature ID.
   *
   * @return string
   *   The Conditional Feature ID as a machine name.
   */
  public function getId();

  /**
   * Returns the Conditional Feature name.
   *
   * @return string
   *   The Conditional Feature name.
   */
  public function getName();

  /**
   * Returns the Conditional Feature description.
   *
   * @return string
   *   The Conditional Feature description.
   */
  public function getDescription();

  /**
   * Returns the current state of the Conditional Feature.
   *
   * @return bool
   *   A boolean value representing current Conditional Feature object state.
   */
  public function isEnabled();

  /**
   * Sets the state of the Conditional Feature.
   */
  public function setEnabled(bool $state);

}
