<?php

namespace Drupal\conditional_features;

use Drupal\conditional_features\Entity\ConditionalFeature;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a list of Conditional Features.
 */
class ConditionalFeaturesListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'name' => $this->t('Name'),
      'id' => $this->t('Machine name'),
      'state' => $this->t('Enabled'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if ($entity instanceof ConditionalFeature) {
      return [
        'name' => $entity->toLink($entity->getName(), 'canonical'),
        'id' => $entity->getId(),
        'state' => $entity->isEnabled() ? $this->t('Yes') : $this->t('No'),
      ] + parent::buildRow($entity);
    }

    return parent::buildRow($entity);
  }

}
