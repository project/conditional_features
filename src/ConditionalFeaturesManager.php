<?php

namespace Drupal\conditional_features;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The Conditional Features Manager service.
 */
class ConditionalFeaturesManager implements ConditionalFeaturesManagerInterface {

  /**
   * The Conditional Features State Manager service.
   *
   * @var \Drupal\conditional_features\ConditionalFeaturesStateManager
   */
  protected ConditionalFeaturesStateManager $conditionalFeaturesStateManager;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a ConditionalFeaturesManager object.
   *
   * @param \Drupal\conditional_features\ConditionalFeaturesStateManager $conditional_features_state_manager
   *   Conditional Features State Manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   */
  public function __construct(ConditionalFeaturesStateManager $conditional_features_state_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->conditionalFeaturesStateManager = $conditional_features_state_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeatures($ids = NULL): array {
    return $this->entityTypeManager->getStorage('conditional_feature')->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledFeatures(): array {
    return array_filter($this->getFeatures(), function ($feature) {
      return $feature->isEnabled();
    });
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled($id): bool {
    return (bool) $this->conditionalFeaturesStateManager->get($id, FALSE);
  }

}
