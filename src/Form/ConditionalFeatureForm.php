<?php

namespace Drupal\conditional_features\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Conditional feature form.
 */
final class ConditionalFeatureForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#maxlength' => EntityTypeInterface::ID_MAX_LENGTH,
      '#machine_name' => [
        'exists' => '\Drupal\conditional_features\Entity\ConditionalFeature::load',
        'source' => ['name'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->isEnabled(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $this->entity->setEnabled((bool) $form_state->getValue('enabled'));

    $message_args = ['%name' => $this->entity->getName()];

    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new conditional feature %name.', $message_args),
        \SAVED_UPDATED => $this->t('Updated conditional feature %name.', $message_args),
      }
    );

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
