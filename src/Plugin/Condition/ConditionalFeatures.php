<?php

namespace Drupal\conditional_features\Plugin\Condition;

use Drupal\conditional_features\ConditionalFeaturesManagerInterface;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Conditional Features condition.
 *
 * @Condition(
 *   id = "conditional_features",
 *   label = @Translation("Conditional Features"),
 * )
 */
class ConditionalFeatures extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Conditional Features Manager service.
   *
   * @var \Drupal\conditional_features\ConditionalFeaturesManagerInterface
   */
  protected $conditionalFeaturesManager;

  /**
   * Constructs a Conditional Features condition plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\conditional_features\ConditionalFeaturesManagerInterface $conditional_features_manager
   *   The Conditional Features Manager.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, ConditionalFeaturesManagerInterface $conditional_features_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->conditionalFeaturesManager = $conditional_features_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('conditional_features.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $has_intersection = (bool) array_intersect(
      $this->configuration['conditional_features'],
      array_keys($this->conditionalFeaturesManager->getEnabledFeatures())
    );

    return $has_intersection;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $enabled_features = array_intersect($this->configuration['conditional_features'], array_keys($this->conditionalFeaturesManager->getEnabledFeatures()));
    $enabled_features = implode(', ', $enabled_features);

    return $this->t('Returns TRUE if any of @enabled_features conditional features are enabled', [
      '@enabled_features' => $enabled_features,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'conditional_features' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['conditional_features'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getFeatures(),
      '#title' => $this->t('Conditional features'),
      '#description' => $this->t('Select conditional features to test'),
      '#default_value' => $this->configuration['conditional_features'],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['conditional_features'] = array_filter($form_state->getValue('conditional_features'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFeatures():array {
    return array_map(function ($conditional_feature) {
      return $conditional_feature->getName();
    }, $this->conditionalFeaturesManager->getFeatures());
  }

}
