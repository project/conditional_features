<?php

namespace Drupal\conditional_features;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\State\State;

/**
 * Provides a class to manage state for Conditional Features.
 */
class ConditionalFeaturesStateManager extends State {

  /**
   * {@inheritdoc}
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory) {
    parent::__construct($key_value_factory);
    $this->keyValueStore = $key_value_factory->get('conditional_features');
  }

}
