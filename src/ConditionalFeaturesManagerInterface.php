<?php

namespace Drupal\conditional_features;

/**
 * Provides an interface defining the Conditional Feature Manager class.
 */
interface ConditionalFeaturesManagerInterface {

  /**
   * Gets all Conditional Feature config entities.
   *
   * @return \Drupal\conditional_features\Entity\ConditionalFeature[]
   *   An array of Conditional Feature config entities.
   */
  public function getFeatures($ids = NULL): array;

  /**
   * Gets a filtered array of Conditional Features config entities.
   *
   * @return array
   *   An array of enabled Conditional Features objects.
   */
  public function getEnabledFeatures(): array;

  /**
   * Test if the current state of a Conditional Feature is enabled.
   *
   * @param string $id
   *   The machine name of a Conditional Feature.
   *
   * @return bool
   *   The enabled state of a Conditional Feature.
   */
  public function isEnabled($id): bool;

}
