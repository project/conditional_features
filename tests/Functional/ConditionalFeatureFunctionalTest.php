<?php

namespace Drupal\Tests\conditional_features\Functional;

use Drupal\conditional_features\Entity\ConditionalFeature;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Defines a class for testing Conditional Features.
 *
 * @group conditional_features
 */
class ConditionalFeatureFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user to test against.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The web assert object.
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected $assertSession;


  /**
   * {@inheritdoc}
   */
  protected static $modules = ['conditional_features', 'block', 'system'];

  /**
   * The Conditional Feature Manager service.
   *
   * @var \Drupal\conditional_features\ConditionalFeaturesManager
   */
  protected $conditionalFeatureManager;

  /**
   * Conditional Feature to utilise throughout tests.
   *
   * @var \Drupal\conditional_features\Entity\ConditionalFeature
   */
  protected $conditionalFeature;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->createUser([
      'administer conditional_feature',
    ]);
    $this->assertSession = $this->assertSession();
    $this->conditionalFeatureManager = \Drupal::service('conditional_features.manager');
  }

  /**
   * Testing conditional feature admin user journey.
   */
  public function testAdminConditionalFeatureUserJourney() {
    $this->adminCreateConditionalFeature();
    $this->adminEditConditionalFeature();
    $this->adminDeleteConditionalFeature();
  }

  /**
   * Testing if an admin user can create a conditional feature.
   */
  public function adminCreateConditionalFeature() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet(Url::fromRoute('entity.conditional_feature.collection'));
    $this->assertSession->statusCodeEquals(200);
    $this->assertSession->pageTextContains('There are no conditional features yet.');

    $this->drupalGet(Url::fromRoute('entity.conditional_feature.add_form'));
    $this->assertSession->statusCodeEquals(200);

    $name = $this->randomMachineName();
    $id = strtolower($name);
    $description = $this->randomMachineName();

    $form_data = [
      'edit-name' => $name,
      'edit-id' => $id,
      'edit-description' => $description,
    ];
    $this->submitForm($form_data, 'Save');
    $this->assertSession->pageTextContains(sprintf('Created new conditional feature %s.', $name));

    $this->conditionalFeature = ConditionalFeature::load($id);

    $this->assertCount(1, $this->conditionalFeatureManager->getFeatures());
    $this->assertCount(0, $this->conditionalFeatureManager->getEnabledFeatures());
  }

  /**
   * Testing if an admin user can edit a conditional feature.
   */
  public function adminEditConditionalFeature() {
    $this->drupalGet(Url::fromRoute('entity.conditional_feature.collection'));
    $this->assertSession->statusCodeEquals(200);
    $this->assertSession->pageTextNotContains('There are no conditional features yet.');
    $this->assertSession->pageTextContains($this->conditionalFeature->getName());

    $this->drupalGet(Url::fromRoute('entity.conditional_feature.edit_form', ['conditional_feature' => $this->conditionalFeature->getId()]));
    $this->assertSession->fieldValueEquals('edit-name', $this->conditionalFeature->getName());
    $this->assertSession->fieldValueEquals('edit-description', $this->conditionalFeature->getDescription());
    $this->assertSession->fieldValueEquals('edit-enabled', $this->conditionalFeature->isEnabled());

    $new_name = $this->randomMachineName();
    $form_data = [
      'edit-name' => $new_name,
      'edit-enabled' => TRUE,
    ];
    $this->assertSession->checkboxNotChecked('edit-enabled');

    $this->submitForm($form_data, 'Save');
    $this->assertSession->pageTextContains(sprintf('Updated conditional feature %s.', $new_name));

    $this->conditionalFeature = ConditionalFeature::load($this->conditionalFeature->getId());
    $this->assertCount(1, $this->conditionalFeatureManager->getFeatures());
  }

  /**
   * Testing if an admin user can edit a conditional feature.
   */
  public function adminDeleteConditionalFeature() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet(Url::fromRoute('entity.conditional_feature.collection'));
    $this->assertSession->statusCodeEquals(200);
    $this->assertSession->pageTextNotContains('There are no conditional features yet.');
    $this->assertSession->pageTextContains($this->conditionalFeature->getName());

    $this->drupalGet(Url::fromRoute('entity.conditional_feature.delete_form', ['conditional_feature' => $this->conditionalFeature->getId()]));
    $this->submitForm([], 'Delete');
    $this->assertSession->pageTextContains(sprintf('The conditional feature %s has been deleted.', $this->conditionalFeature->getName()));
  }

}
