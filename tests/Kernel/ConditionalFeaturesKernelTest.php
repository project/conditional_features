<?php

namespace Drupal\Tests\conditional_features\Kernel;

use Drupal\conditional_features\Entity\ConditionalFeature;
use Drupal\KernelTests\KernelTestBase;

/**
 * Defines a class for testing Conditional Features.
 *
 * @group conditional_features
 */
class ConditionalFeaturesKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'conditional_features',
    'system',
  ];

  /**
   * The Conditional Feature Manager service.
   *
   * @var \Drupal\conditional_features\ConditionalFeaturesManager
   */
  protected $conditionalFeatureManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->conditionalFeatureManager = \Drupal::service('conditional_features.manager');
  }

  /**
   * Testing Conditional Feature Manager.
   */
  public function testConditionalFeatureManager(): void {
    $id = $this->randomMachineName();

    // Test our list of features is empty.
    $this->assertCount(0, $this->conditionalFeatureManager->getFeatures());

    // Test our list of enabled features is empty.
    $this->assertCount(0, $this->conditionalFeatureManager->getEnabledFeatures());

    // Create a conditional feature to test against.
    $conditional_feature = ConditionalFeature::create([
      'id' => $id,
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
    ]);
    $conditional_feature->save();

    // Test that conditional feature is disabled by default.
    $this->assertFalse($this->conditionalFeatureManager->isEnabled($id));

    // Test that it appears in the list of conditional features.
    $this->assertCount(1, $this->conditionalFeatureManager->getFeatures());

    // Test that it doesn't appear in the list of enabled conditional features.
    $this->assertCount(0, $this->conditionalFeatureManager->getEnabledFeatures());

    // Enable feature and test it appears in list of enabled features.
    $conditional_feature->setEnabled(TRUE);
    $this->assertTrue($this->conditionalFeatureManager->isEnabled($id));
    $this->assertCount(1, $this->conditionalFeatureManager->getEnabledFeatures());

    // Test disabled feature doesn't appears in list of features.
    $conditional_feature->setEnabled(FALSE);
    $this->assertFalse($this->conditionalFeatureManager->isEnabled($id));
    $this->assertCount(0, $this->conditionalFeatureManager->getEnabledFeatures());
  }

}
